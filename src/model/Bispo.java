package model;

import java.awt.Point;
import java.util.ArrayList;

public class Bispo extends Piece {

	private ArrayList<Point> moves;

        public Bispo(Team team) {
            super(team);
            this.moves = new ArrayList<>();
        }
        
	public Bispo(Team team, boolean itMoved) {
		super(team, itMoved);
		this.moves = new ArrayList<>();
	}
        
        @Override
        public String obtainPath(){
                String color = this.getTeam() == Piece.Team.UP_TEAM? "Black" : "White";
                String path = "icon/" + color + " B_48x48.png";
                return path;
        }

	@Override
	public ArrayList<Point> getMoves(int x, int y, ArrayList<Square> squareList) {
		this.moves.clear();
                Square square;
                
		for (int i = y+1; Validate(i, x+i-y); i++) {
                    square = squareList.get(i*CHESSBOARD_COL+x+i-y);
                    if(!this.testPoint(square, x+i-y, i, moves)){
                        break;
                    }
                }
		
                for(int i = y-1; Validate(i, x+i-y); i--){
                    square = squareList.get(i*CHESSBOARD_COL+x+i-y);
                    if(!this.testPoint(square, x+i-y, i, moves)){
                        break;
                    }
                }
                
		for (int i = y+1; Validate(i, x-i+y); i++) {
                    square = squareList.get(i*CHESSBOARD_COL+x-i+y);
                    if(!this.testPoint(square, x-i+y, i, moves)){
                        break;
                    }
		}
                
                for (int i = y-1; Validate(i, x-i+y); i--) {
                    square = squareList.get(i*CHESSBOARD_COL+x-i+y);
                    if(!this.testPoint(square, x-i+y, i, moves)){
                        break;
                    }
                }
                
		return this.moves;
	}
}