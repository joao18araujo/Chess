package model;

import java.awt.Point;
import java.util.ArrayList;

public class Peao extends Piece {

	private final ArrayList<Point> moves;

        public Peao(Team team) {
            super(team);
            this.moves = new ArrayList<>();
        }
        
	public Peao(Team team, boolean itMoved) {
            super(team, itMoved);
            this.moves = new ArrayList<>();
	}
        
        public String obtainPath(){
                String color = this.getTeam() == Piece.Team.UP_TEAM? "Black" : "White";
                String path = "icon/" + color + " P_48x48.png";
                return path;
        }

	@Override
	public ArrayList<Point> getMoves(int x, int y, ArrayList<Square> squareList) {
		this.moves.clear();
                int operator = getTeam() == Team.UP_TEAM? 1 : -1; 
                Square square;
        
                Point point;
                boolean v = true;
                
                if(Validate(x, y+operator)){
                    square = squareList.get((y+operator)*CHESSBOARD_COL + x);
                    if(!square.havePiece()){
                        point = new Point(x, y + operator);
                        this.moves.add(point);
                    } else {
                        v = false;
                    }
                }

                if(v){
                    if(Validate(x, y+2*operator)){
                        square = squareList.get((y+2*operator)*CHESSBOARD_COL + x);
                        if (!this.itMoved()){ 
                            if(!square.havePiece()){
                                point = new Point(x, y + 2*operator);
             
                                this.moves.add(point);
                            }
                        }
                    }
                }
                
                if(Validate(x+1, y+operator)){
                    square = squareList.get((y+operator)*CHESSBOARD_COL + x+1);
                    if(square.havePiece()){
                        if(this.getTeam() != square.getPiece().getTeam()){
                            point = new Point(x+1, y+operator);
                            this.moves.add(point);
                        }
                    }
                }
                
                if(Validate(x-1, y+operator)){
                    square = squareList.get((y+operator)*CHESSBOARD_COL + x-1);
                    if(square.havePiece()){
                        if(this.getTeam() != square.getPiece().getTeam()){
                            point = new Point(x-1, y+operator);
                            this.moves.add(point);
                        }
                    }
                }
                    
		return this.moves;
	}
        
        @Override
        public ArrayList<Point> getPassantMove(int x, int y, ArrayList<Square> squareList, ArrayList<Point> lastMove){
            ArrayList<Point> move = new ArrayList<>();
            int operator = getTeam() == Team.UP_TEAM? 1 : -1;
            Square square;
            
            if((y == 3 && this.getTeam() == Team.DOWN_TEAM) || (y == 4 && this.getTeam() == Team.UP_TEAM)){
                if(Validate(x-1, y+2*operator)){
                    square = squareList.get((y+2*operator)*CHESSBOARD_COL + x-1);
                    if(lastMove.contains(square.getPosition())){
                        square = squareList.get(y*CHESSBOARD_COL + x-1);
                        if(lastMove.contains(square.getPosition())){
                            square = squareList.get(y*CHESSBOARD_COL + x-1);
                            if(square.havePiece()){
                                if(square.getPiece().getClass() == Peao.class){
                                    square = squareList.get((y+operator)*CHESSBOARD_COL + x-1);
                                    move.add(square.getPosition());
                                }
                            }
                        }
                    }
                }
                
                if(Validate(x+1, y+2*operator)){
                    square = squareList.get((y+2*operator)*CHESSBOARD_COL + x+1);
                    if(lastMove.contains(square.getPosition())){
                        square = squareList.get(y*CHESSBOARD_COL + x+1);
                        if(lastMove.contains(square.getPosition())){
                            square = squareList.get(y*CHESSBOARD_COL + x+1);
                            if(square.havePiece()){
                                if(square.getPiece().getClass() == Peao.class){
                                    square = squareList.get((y+operator)*CHESSBOARD_COL + x+1);
                                    move.add(square.getPosition());
                                }
                            }
                        }
                    }
                }
            }
            
            return move;
        }
}
