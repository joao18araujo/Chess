package model;

import control.TreatControl;
import java.awt.Point;
import java.util.ArrayList;

public class Rei extends Piece {

	private ArrayList<Point> moves;

        public Rei(Team team) {
            super(team);
            this.moves = new ArrayList<>();
        }
        
	public Rei(Team team, boolean itMoved) {
		super(team, itMoved);
		this.moves = new ArrayList<>();
	}

        @Override
        public String obtainPath(){
                String color = this.getTeam() == Piece.Team.UP_TEAM? "Black" : "White";
                String path = "icon/" + color + " K_48x48.png";
                return path;
        }
        
	@Override
	public ArrayList<Point> getMoves(int x, int y, ArrayList<Square> squareList) {
		this.moves.clear();

		Square square;
		if(Validate(x+1, y)){
                    square = squareList.get(y*CHESSBOARD_COL+x+1);
                    this.testPoint(square, x+1, y, moves);
                }
                if(Validate(x-1, y)){
                    square = squareList.get(y*CHESSBOARD_COL+x-1);
                    this.testPoint(square, x-1, y, moves);
                }
                if(Validate(x, y+1)){
                    square = squareList.get((y+1)*CHESSBOARD_COL+x);
                    this.testPoint(square, x, y+1, moves);
                }
                if(Validate(x, y-1)){
                    square = squareList.get((y-1)*CHESSBOARD_COL+x);
                    this.testPoint(square, x, y-1, moves);
                }
                if(Validate(x+1, y+1)){
                    square = squareList.get((y+1)*CHESSBOARD_COL+x+1);
                    this.testPoint(square, x+1, y+1, moves);
                }
                if(Validate(x+1, y-1)){
                    square = squareList.get((y-1)*CHESSBOARD_COL+x+1);
                    this.testPoint(square, x+1, y-1, moves);
                }
                if(Validate(x-1, y+1)){
                    square = squareList.get((y+1)*CHESSBOARD_COL+x-1);
                    this.testPoint(square, x-1, y+1, moves);
                }
                if(Validate(x-1, y-1)){
                    square = squareList.get((y-1)*CHESSBOARD_COL+x-1);
                    this.testPoint(square, x-1, y-1, moves);
                }

		return this.moves;
	}
        
        @Override
        public ArrayList<Point> getCastlingMoves(int x, int y, ArrayList<Square> squareList) {
            ArrayList<Point> castlingMoves = new ArrayList<>();
            ArrayList<Point> treats;
            
            Square principalSquare = squareList.get(y*CHESSBOARD_COL+x);
            treats = TreatControl.treatControl(x, y, squareList);
            
            if(!principalSquare.getPiece().itMoved() && treats.isEmpty()){
                Square square = squareList.get(y*CHESSBOARD_COL+x+3);
                if(square.havePiece()){
                    treats = TreatControl.treatControl(x+1, y, squareList);
                    Square emptySquare = squareList.get(y*CHESSBOARD_COL+x+1);
                    if(treats.isEmpty() && !emptySquare.havePiece()){
                        treats = TreatControl.treatControl(x+2, y, squareList);
                        emptySquare = squareList.get(y*CHESSBOARD_COL+x+2);
                        if(treats.isEmpty() && !emptySquare.havePiece()){
                            castlingMoves.add(new Point(x+3, y));
                        }
                    }
                }
            }

            if(!principalSquare.getPiece().itMoved()){
                Square square = squareList.get(y*CHESSBOARD_COL+x-4);
                if(square.havePiece()){
                    treats = TreatControl.treatControl(x-1, y, squareList);
                    Square emptySquare = squareList.get(y*CHESSBOARD_COL+x-1);
                    if(treats.isEmpty() && !emptySquare.havePiece()){
                        treats = TreatControl.treatControl(x-2, y, squareList);
                        emptySquare = squareList.get(y*CHESSBOARD_COL+x-2);
                        if(treats.isEmpty() && !emptySquare.havePiece()){
                            emptySquare = squareList.get(y*CHESSBOARD_COL+x-3);
                            if(!emptySquare.havePiece()){
                                castlingMoves.add(new Point(x-4, y));
                            }
                        }
                    }
                }
            }
            
            return castlingMoves;
        }
}
