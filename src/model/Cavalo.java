package model;

import java.awt.Point;
import java.util.ArrayList;

public class Cavalo extends Piece {

	private ArrayList<Point> moves;

        public Cavalo(Team team) {
            super(team);
            this.moves = new ArrayList<>();
        }
        
	public Cavalo(Team team, boolean itMoved) {
		super(team, itMoved);
		this.moves = new ArrayList<>();
	}
        
        @Override
        public String obtainPath(){
                String color = this.getTeam() == Piece.Team.UP_TEAM? "Black" : "White";
                String path = "icon/" + color + " N_48x48.png";
                return path;
        }

	@Override
	public ArrayList<Point> getMoves(int x, int y, ArrayList<Square> squareList) {
		this.moves.clear();

                Square square;
		if(Validate(x+1, y+2)){
                    square = squareList.get((y+2)*CHESSBOARD_COL+x+1);
                    this.testPoint(square, x+1, y+2, moves);
                }
                if(Validate(x+1, y-2)){
                    square = squareList.get((y-2)*CHESSBOARD_COL+x+1);
                    this.testPoint(square, x+1, y-2, moves);
                }
                if(Validate(x-1, y+2)){
                    square = squareList.get((y+2)*CHESSBOARD_COL+x-1);
                    this.testPoint(square, x-1, y+2, moves);
                }
                if(Validate(x-1, y-2)){
                    square = squareList.get((y-2)*CHESSBOARD_COL+x-1);
                    this.testPoint(square, x-1, y-2, moves);
                }
                if(Validate(x+2, y+1)){
                    square = squareList.get((y+1)*CHESSBOARD_COL+x+2);
                    this.testPoint(square, x+2, y+1, moves);
                }
                if(Validate(x+2, y-1)){
                    square = squareList.get((y-1)*CHESSBOARD_COL+x+2);
                    this.testPoint(square, x+2, y-1, moves);
                }
                if(Validate(x-2, y+1)){
                    square = squareList.get((y+1)*CHESSBOARD_COL+x-2);
                    this.testPoint(square, x-2, y+1, moves);
                }
                if(Validate(x-2, y-1)){
                    square = squareList.get((y-1)*CHESSBOARD_COL+x-2);
                    this.testPoint(square, x-2, y-1, moves);
                }
                

		return this.moves;
	}
}
