package model;

import java.awt.Point;
import java.util.ArrayList;

public abstract class Piece {

	public static final int CHESSBOARD_ROW = 8;
	public static final int CHESSBOARD_COL = 8;

	private Team team;
        private boolean itMoved;

	public static enum Team {
		UP_TEAM, DOWN_TEAM;
	}

        public Piece(Team team) {
                this.team = team;
                this.itMoved = false;
        }
        
	public Piece(Team team, boolean itMoved) {
		this.team = team;
                this.itMoved = itMoved;
	}
        
        public abstract String obtainPath();

	public abstract ArrayList<Point> getMoves(int x, int y, ArrayList<Square> squareList);

        public ArrayList<Point> getCastlingMoves(int x, int y, ArrayList<Square> squareList) {
            ArrayList<Point> castlingMoves = new ArrayList<>();
            return castlingMoves;
        }
        
        public ArrayList<Point> getPassantMove(int x, int y, ArrayList<Square> squareList, ArrayList<Point> lastMove){
            ArrayList<Point> move = new ArrayList<>();
            return move;
        }
        
	public boolean pieceIsOnMyTeam(Piece piece) {
		return this.team == piece.getTeam();
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}
        
        public boolean itMoved() {
            return itMoved;
        }
        
        public void setItMoved(boolean itMoved) {
            this.itMoved = itMoved;
        }
        
        public static boolean Validate(int x, int y){
            return x>=0 && x < CHESSBOARD_COL && y >=0 && y < CHESSBOARD_ROW;
        }
        
        public boolean testPoint(Square square, int x, int y, ArrayList<Point> moves){
            if(square.havePiece()){
                if(this.getTeam() != square.getPiece().getTeam()){
                    moves.add(new Point(x, y));
                }
                return false;
            }else{
                moves.add(new Point(x, y));
            }
            
            return true;
        }
        
}

