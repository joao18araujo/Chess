package model;

import control.TreatControl;
import java.awt.Point;
import java.util.ArrayList;

public class Torre extends Piece {

	private ArrayList<Point> moves;

        public Torre(Team team) {
            super(team);
            this.moves = new ArrayList<>();
        }
        
	public Torre(Team team, boolean itMoved) {
		super(team, itMoved);
		this.moves = new ArrayList<>();
	}

        @Override
        public String obtainPath(){
                String color = this.getTeam() == Piece.Team.UP_TEAM? "Black" : "White";
                String path = "icon/" + color + " R_48x48.png";
                return path;
        }
        
	@Override
	public ArrayList<Point> getMoves(int x, int y, ArrayList<Square> squareList) {
		this.moves.clear();
               
                Square square;
                
		for (int i = y+1; i < CHESSBOARD_ROW; i++) {
                    square = squareList.get(i*CHESSBOARD_COL+x);
                    if(!this.testPoint(square, x, i, this.moves)){
                        break;
                    }
		}
		
                for(int i = y-1; i >=0; i--){
                    square = squareList.get(i*CHESSBOARD_COL+x);
                    if(!this.testPoint(square, x, i, this.moves)){
                        break;
                    }
                }
                
		for (int i = x+1; i < CHESSBOARD_COL; i++) {
                    square = squareList.get(y*CHESSBOARD_COL+i);
                    if(!this.testPoint(square, i, y, this.moves)){
                        break;
                    }
		}
                
                for (int i = x-1; i >= 0; i--) {
                    square = squareList.get(y*CHESSBOARD_COL+i);
                    if(!this.testPoint(square, i, y, this.moves)){
                        break;
                    }
                }
                
		return this.moves;
	}
        
        @Override
        public ArrayList<Point> getCastlingMoves(int x, int y, ArrayList<Square> squareList) {
            ArrayList<Point> castlingMoves = new ArrayList<>();
            
            Square principalSquare = squareList.get(y*CHESSBOARD_COL+x);
            Square square;
            ArrayList<Point> treats;
            
            if(!principalSquare.getPiece().itMoved()){
                if(x == 7){
                    square = squareList.get(y*CHESSBOARD_COL+x-3);
                    treats = TreatControl.treatControl(x-3, y, squareList);
                    if(square.havePiece() && treats.isEmpty()){
                        if(!square.getPiece().itMoved()){
                            treats = TreatControl.treatControl(x-1, y, squareList);
                            Square emptySquare = squareList.get(y*CHESSBOARD_COL+x-1);
                            if(treats.isEmpty() && !emptySquare.havePiece()){
                                treats = TreatControl.treatControl(x-2, y, squareList);
                                emptySquare = squareList.get(y*CHESSBOARD_COL+x-2);
                                if(treats.isEmpty() && !emptySquare.havePiece()){
                                    castlingMoves.add(new Point(x-3, y));
                                }
                            }
                        }
                    }
                }else {
                    square = squareList.get(y*CHESSBOARD_COL+x+4);
                    treats = TreatControl.treatControl(x+4, y, squareList);
                    if(square.havePiece() && treats.isEmpty()){
                        treats = TreatControl.treatControl(x+3, y, squareList);
                        Square emptySquare = squareList.get(y*CHESSBOARD_COL+x+3);
                        if(treats.isEmpty() && !emptySquare.havePiece()){
                            treats = TreatControl.treatControl(x+2, y, squareList);
                            emptySquare = squareList.get(y*CHESSBOARD_COL+x+2);
                            if(treats.isEmpty() && !emptySquare.havePiece()){
                                emptySquare = squareList.get(y*CHESSBOARD_COL+x+1);
                                if(!emptySquare.havePiece()){
                                    castlingMoves.add(new Point(x+4, y));
                                }
                            }
                        }
                    }
                }
            }
            
            return castlingMoves;
        }
}
