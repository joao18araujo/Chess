package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import model.Square;
import control.SquareControl;
import model.Bispo;
import model.Cavalo;
import model.Peao;
import model.Piece;
import model.Rainha;
import model.Rei;
import model.Torre;

public class SquareBoardPanel extends JPanel {

	private static final long serialVersionUID = 7332850110063699836L;

	private SquareControl squareControl;
	private ArrayList<SquarePanel> squarePanelList;

	public SquareBoardPanel() {
		setLayout(new GridBagLayout());
		this.squarePanelList = new ArrayList<>();

		initializeSquareControl();
		initializePiecesInChess();
                initializeGrid();
	}

	private void initializeSquareControl() {
		Color colorOne = new Color(255, 235, 205);
		Color colorTwo = new Color(120, 60, 19);
		Color colorHover = Color.GREEN;
		Color colorOneSelected = Color.CYAN;
                Color colorTwoSelected = Color.BLUE;

		this.squareControl = new SquareControl(colorOne, colorTwo, colorHover,
				colorOneSelected, colorTwoSelected);
                this.squareControl.setDownTeam(true);
	}

	private void initializeGrid() {
		GridBagConstraints gridBag = new GridBagConstraints();

		Square square;
		for (int i = 0; i < this.squareControl.getSquareList().size(); i++) {
                        square = this.squareControl.getSquareList().get(i);
			gridBag.gridx = square.getPosition().y;
			gridBag.gridy = square.getPosition().x;
                        int x = square.getPosition().x;
			square.getPosition().setLocation(gridBag.gridx, gridBag.gridy);
                        if(x == 2 || x == 3 || x == 4 || x == 5){
                            square.setHavePiece(false);
                        }
                        
			SquarePanel squarePanel = new SquarePanel(square);

			add(squarePanel, gridBag);
			this.squarePanelList.add(squarePanel);
		}

	}

	private void initializePiecesInChess() {
                Piece piece;
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
                        piece = new Peao(Piece.Team.UP_TEAM, false);
                        this.squareControl.getSquare(1, i).setPiece(piece);
                        this.squareControl.getSquare(1, i).setHavePiece(true);
		}
                piece = new Torre(Piece.Team.UP_TEAM, false);
                this.squareControl.getSquare(0, 0).setPiece(piece);
                this.squareControl.getSquare(0, 0).setHavePiece(true);
                this.squareControl.getSquare(0, 7).setPiece(piece);
                this.squareControl.getSquare(0, 7).setHavePiece(true);
                
                piece = new Cavalo(Piece.Team.UP_TEAM, false);
                this.squareControl.getSquare(0, 1).setPiece(piece);
                this.squareControl.getSquare(0, 1).setHavePiece(true);
                this.squareControl.getSquare(0, 6).setPiece(piece);
                this.squareControl.getSquare(0, 6).setHavePiece(true);
                
                piece = new Bispo(Piece.Team.UP_TEAM, false);
                this.squareControl.getSquare(0, 2).setPiece(piece);
                this.squareControl.getSquare(0, 2).setHavePiece(true);
                this.squareControl.getSquare(0, 5).setPiece(piece);
                this.squareControl.getSquare(0, 5).setHavePiece(true);

                piece = new Rainha(Piece.Team.UP_TEAM, false);
                this.squareControl.getSquare(0, 3).setPiece(piece);
                this.squareControl.getSquare(0, 3).setHavePiece(true);

                piece = new Rei(Piece.Team.UP_TEAM, false);
                this.squareControl.getSquare(0, 4).setPiece(piece);
                this.squareControl.getSquare(0, 4).setHavePiece(true);

		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
                        piece = new Peao(Piece.Team.DOWN_TEAM, false);
                        this.squareControl.getSquare(6, i).setPiece(piece);
                        this.squareControl.getSquare(6, i).setHavePiece(true);
		}
		
                piece = new Torre(Piece.Team.DOWN_TEAM, false);
                this.squareControl.getSquare(7, 0).setPiece(piece);
                this.squareControl.getSquare(7, 0).setHavePiece(true);
                this.squareControl.getSquare(7, 7).setPiece(piece);
                this.squareControl.getSquare(7, 7).setHavePiece(true);
                
                piece = new Cavalo(Piece.Team.DOWN_TEAM, false);
                this.squareControl.getSquare(7, 1).setPiece(piece);
                this.squareControl.getSquare(7, 1).setHavePiece(true);
                this.squareControl.getSquare(7, 6).setPiece(piece);
                this.squareControl.getSquare(7, 6).setHavePiece(true);
                
                piece = new Bispo(Piece.Team.DOWN_TEAM, false);
                this.squareControl.getSquare(7, 2).setPiece(piece);
                this.squareControl.getSquare(7, 2).setHavePiece(true);
                this.squareControl.getSquare(7, 5).setPiece(piece);
                this.squareControl.getSquare(7, 5).setHavePiece(true);
                
                piece = new Rainha(Piece.Team.DOWN_TEAM, false);
                this.squareControl.getSquare(7, 3).setPiece(piece);
                this.squareControl.getSquare(7, 3).setHavePiece(true);
                
                piece = new Rei(Piece.Team.DOWN_TEAM, false);
                this.squareControl.getSquare(7, 4).setPiece(piece);
                this.squareControl.getSquare(7, 4).setHavePiece(true);
	}
}
