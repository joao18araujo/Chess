package util;

public class WrongSquareException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public WrongSquareException() { 
		super(); 
	}
	
	public WrongSquareException(String message) { 
		super(message); 
	}
	
	public WrongSquareException(String message, Throwable cause) { 
		super(message, cause); 
	}
	public WrongSquareException(Throwable cause) {
		super(cause); 
	}
}
