package control;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import model.Bispo;
import model.Cavalo;
import model.Peao;
import model.Piece;
import model.Rainha;
import model.Rei;
import model.Torre;

import model.Square;
import model.Square.SquareEventListener;
import util.WrongSquareException;

public class SquareControl implements SquareEventListener {

    public static final int ROW_NUMBER = 8;
    public static final int COL_NUMBER = 8;

    public static final Color DEFAULT_COLOR_ONE = new Color(255, 235, 205);
    public static final Color DEFAULT_COLOR_TWO = new Color(120, 60, 19);
    public static final Color DEFAULT_COLOR_HOVER = Color.GREEN;
    public static final Color DEFAULT_COLOR_ONE_SELECTED = Color.CYAN;
    public static final Color DEFAULT_COLOR_TWO_SELECTED = Color.BLUE;

    public static final Square EMPTY_SQUARE = null;

    private Color colorOne;
    private Color colorTwo;
    private Color colorHover;
    private Color colorOneSelected;
    private Color colorTwoSelected;

    private Square selectedSquare;
    private ArrayList<Square> squareList;
    private ArrayList<Point> pointList;
    private ArrayList<Point> castlingList;
    private ArrayList<Point> passantList;
    private ArrayList<Point> lastMove;
    private boolean DownTeam;
    private Point downKingPosition;
    private Point upKingPosition;

    public SquareControl() {
        this(DEFAULT_COLOR_ONE, DEFAULT_COLOR_TWO, DEFAULT_COLOR_HOVER,
                DEFAULT_COLOR_ONE_SELECTED, DEFAULT_COLOR_TWO_SELECTED);
        this.pointList = new ArrayList<>();
        this.DownTeam = true;
    }

    public SquareControl(Color colorOne, Color colorTwo, Color colorHover,
            Color colorOneSelected, Color colorTwoSelected) {
        this.colorOne = colorOne;
        this.colorTwo = colorTwo;
        this.colorHover = colorHover;
        this.colorOneSelected = colorOneSelected;
        this.colorTwoSelected = colorTwoSelected;

        this.squareList = new ArrayList<>();
        createSquares();
        this.pointList = new ArrayList<>();
        this.castlingList = new ArrayList<>();
        this.passantList = new ArrayList<>();
        this.lastMove = new ArrayList<>();
        this.downKingPosition = new Point(4, 7);
        this.upKingPosition = new Point(4, 0);
    }

    public void resetColor(Square square) {
        int index = this.squareList.indexOf(square);
        int row = index / COL_NUMBER;
        int col = index % COL_NUMBER;

        square.setColor(getGridColor(row, col));
    }

    @Override
    public void onHoverEvent(Square square) {
        square.setColor(this.colorHover);
    }

    @Override
    public void onSelectEvent(Square square) {
        if (haveSelectedCellPanel()) {
            if (!this.selectedSquare.equals(square)) {
                if (this.selectedSquare.havePiece()) {
                    int x = selectedSquare.getPosition().x;
                    int y = selectedSquare.getPosition().y;
                    pointList = this.selectedSquare.getPiece().getMoves(x, y, this.squareList);
                    castlingList = this.selectedSquare.getPiece().getCastlingMoves(x, y, this.squareList);
                    passantList = this.selectedSquare.getPiece().getPassantMove(x, y, squareList, lastMove);
                    
                    try {
                        this.isOnList(square);
                    } catch (WrongSquareException e) {
                        if(square.havePiece()){
                            if(square.getPiece().getTeam() == this.selectedSquare.getPiece().getTeam()){
                                this.unselectSquare(this.selectedSquare);
                                this.squareTest(square);
                            }else{
                                JOptionPane.showMessageDialog(null, "Você não pode capturar essa peça", "Movimento Errado", JOptionPane.WARNING_MESSAGE);
                            }
                        }else{
                            this.unselectSquare(this.selectedSquare);
                        }
                    }
                }
            } else {
                this.unselectSquare(square);
            }

        } else {
            this.squareTest(square);
        }
    }

    @Override
    public void onOutEvent(Square square) {
        int index = this.squareList.indexOf(square);
        int row = index / COL_NUMBER;
        int col = index % COL_NUMBER;

        Point point = new Point(col, row);
        if (!pointList.contains(point) 
                && !castlingList.contains(point) 
                && !passantList.contains(point)) {
            if (this.selectedSquare != square) {
                this.resetColor(square);
            } else {
                this.selectedSquare.setColor(Color.MAGENTA);
            }
        } else {
            this.possibleSquareColor(square, row, col);
        }
    }

    public Square getSquare(int row, int col) {
        return this.squareList.get((row * COL_NUMBER) + col);
    }

    public ArrayList<Square> getSquareList() {
        return this.squareList;
    }

    public Color getGridColor(int row, int col) {
        if ((row + col) % 2 == 0) {
            return this.colorOne;
        } else {
            return this.colorTwo;
        }
    }

    private void addSquare() {
        Square square = new Square();
        this.squareList.add(square);
        this.resetColor(square);
        this.resetPosition(square);
        square.setSquareEventListener(this);
    }

    private void resetPosition(Square square) {
        int index = this.squareList.indexOf(square);
        int row = index / COL_NUMBER;
        int col = index % COL_NUMBER;

        square.getPosition().setLocation(row, col);
    }

    private boolean haveSelectedCellPanel() {
        return this.selectedSquare != EMPTY_SQUARE;
    }

    private void moveContentOfSelectedSquare(Square square) {

        Piece piece = null;
        boolean v, itMoved;
        if(square.havePiece()){
            piece = square.getPiece();
        }
        itMoved = this.selectedSquare.getPiece().itMoved();
        this.selectedSquare.getPiece().setItMoved(true);
        square.setPiece(this.selectedSquare.getPiece());
        square.setHavePiece(true);
        this.selectedSquare.setHavePiece(false);
        this.updateKingsPosition(square);
               
        try {
            checkTest();
        } catch (WrongSquareException e) {
            AutoCheck(square, piece, itMoved, e);
            return;
        }

        promotionTest(square);
        try {
            v = checkTest();
        } catch (WrongSquareException ex) {
            return;
        }
        
        this.selectedSquare.notifyOnChangeImagePath();
        square.notifyOnChangeImagePath();
        this.DownTeam = !this.DownTeam;
        this.lastMove.clear();
        this.lastMove.add(this.selectedSquare.getPosition());
        this.lastMove.add(square.getPosition());
        this.unselectSquare(square);
        
        consultCheckmate(v);
    }

    private void selectSquare(Square square) {
        int row, col;

        if (square.havePiece()) {
            this.selectedSquare = square;
            int x = this.selectedSquare.getPosition().x;
            int y = this.selectedSquare.getPosition().y;
            pointList = this.selectedSquare.getPiece().getMoves(x, y, this.squareList);
            castlingList = this.selectedSquare.getPiece().getCastlingMoves(x, y, this.squareList);
            passantList = this.selectedSquare.getPiece().getPassantMove(x, y, squareList, lastMove);
            
            this.selectedSquare.setColor(Color.MAGENTA);
            for (Point point : pointList) {
                row = point.y;
                col = point.x;
                this.possibleSquareColor(this.getSquare(row, col), row, col);
            }
            
            for(Point point : castlingList) {
                row = point.y;
                col = point.x;
                this.possibleSquareColor(this.getSquare(row, col), row, col);
            }
            
            for(Point point : passantList) {
                row = point.y;
                col = point.x;
                this.possibleSquareColor(this.getSquare(row, col), row, col);
            }
        }
    }

    private void unselectSquare(Square square) {
        resetColor(this.selectedSquare);

        this.selectedSquare = EMPTY_SQUARE;
        for (Point point : this.pointList) {
            int x = point.x;
            int y = point.y;
            resetColor(getSquare(y, x));
        }
        pointList.clear();
        
        for (Point point : this.castlingList) {
            int x = point.x;
            int y = point.y;
            resetColor(getSquare(y, x));
        }
        castlingList.clear();
        
        for(Point point : this.passantList) {
            int x = point.x;
            int y = point.y;
            resetColor(getSquare(y, x));
        }
        passantList.clear();
    }

    private void createSquares() {
        for (int i = 0; i < (ROW_NUMBER * COL_NUMBER); i++) {
            addSquare();
        }
    }

    public boolean isDownTeam() {
        return DownTeam;
    }

    public void setDownTeam(boolean DownTeam) {
        this.DownTeam = DownTeam;
    }

    public void isOnList(Square square) throws WrongSquareException {
        if (pointList.contains(square.getPosition())) {
            moveContentOfSelectedSquare(square);
        } else {
            if(castlingList.contains(square.getPosition())){
                executeCastling(square.getPosition());
            }else {
                if(passantList.contains(square.getPosition())){
                    executePassant(square.getPosition());
                }else{
                    throw new WrongSquareException();
                }
            }
        }
    }

    public void turnTest(Square square) throws WrongSquareException {
        Piece.Team team = square.getPiece().getTeam();
        if (team == Piece.Team.DOWN_TEAM ^ this.DownTeam) {
            String message = this.DownTeam ? "Turno do Time Branco" : "Turno do Time Preto";
            throw new WrongSquareException(message);
        } else {
            selectSquare(square);
        }
    }

    public void squareTest(Square square) {
        if (square.havePiece()) {
            try {
                turnTest(square);
            } catch (WrongSquareException e) {
                JOptionPane.showMessageDialog(null, e.getMessage(), "Movimento Errado", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    public void possibleSquareColor(Square square, int row, int col) {
        if (square.havePiece()) {
            if(pointList.contains(new Point(col, row))){
                if ((getGridColor(row, col) == this.colorOne)) {
                    this.getSquare(row, col).setColor(new Color(255, 0, 0));
                } else {
                    this.getSquare(row, col).setColor(new Color(210, 0, 0));
                }
            }else {
                if ((getGridColor(row, col) == this.colorOne)) {
                    this.getSquare(row, col).setColor(Color.YELLOW);
                } else {
                    this.getSquare(row, col).setColor(new Color(204, 204, 0));
                }
            }
        } else {
            if(passantList.contains(new Point(col, row))){
                if ((getGridColor(row, col) == this.colorOne)) {
                    this.getSquare(row, col).setColor(new Color(140, 140, 140));
                } else {
                    this.getSquare(row, col).setColor(Color.BLACK);
                }
            } else {
                if (getGridColor(row, col) == this.colorOne) {
                    square.setColor(this.colorOneSelected);
                } else {
                    square.setColor(this.colorTwoSelected);
                }
            }
        }
    }

    public boolean checkTest() throws WrongSquareException {
        int x = DownTeam? downKingPosition.x : upKingPosition.x;
        int y = DownTeam? downKingPosition.y : upKingPosition.y;
        String message = DownTeam? "Time Branco" : "Time Preto";
        ArrayList<Point> treats = TreatControl.treatControl(x, y, squareList);
        if(!treats.isEmpty()){
            throw new WrongSquareException(message);
        }
        else{
            x = DownTeam? upKingPosition.x : downKingPosition.x;
            y = DownTeam? upKingPosition.y : downKingPosition.y;
            
            treats = TreatControl.treatControl(x, y, squareList);
            if (!treats.isEmpty()) {
                return true;
            }
            return false;
        }
    }
    
    public boolean checkmateTest(){
        Square square;
        
        for(int i = 0; i < ROW_NUMBER; i++){
            for(int j = 0; j < COL_NUMBER; j++){
                square = this.getSquare(i, j);
                if(square.havePiece()){
                    if(square.getPiece().getTeam() == Piece.Team.UP_TEAM ^ this.DownTeam){
                        ArrayList<Point> moves = square.getPiece().getMoves(j, i, this.squareList);
                        for(Point move : moves){
                            if(SimulateMove(square, move)){
                                System.out.println("Possível movimento: " + square.getPosition() + "" +move);
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
    
    public void updateKingsPosition(Square square){
        if(square.getPiece().getClass() == Rei.class){
            if(DownTeam){
                downKingPosition = square.getPosition();
            }else{
                upKingPosition = square.getPosition();
            }
        }
    }
    
    public boolean SimulateMove(Square selectedSquare, Point squarePoint){
        Square square = this.getSquare(squarePoint.y, squarePoint.x);
        Piece piece = null;
        if(square.havePiece()){
            piece = square.getPiece();
        }
        square.setPiece(selectedSquare.getPiece());
        square.setHavePiece(true);
        selectedSquare.setHavePiece(false);
        this.updateKingsPosition(square);
        
        try {
            checkTest();
        } catch (WrongSquareException e) {
            
            selectedSquare.setPiece(square.getPiece());
            selectedSquare.setHavePiece(true);
            this.updateKingsPosition(selectedSquare);
            
            if(piece != null){
                square.setPiece(piece);
                square.setHavePiece(true);
                this.updateKingsPosition(square);
            }else{
                square.setHavePiece(false);
            }
            return false;
        }
        selectedSquare.setPiece(square.getPiece());
        selectedSquare.setHavePiece(true);
        this.updateKingsPosition(selectedSquare);
            
        if(piece != null){
        square.setPiece(piece);
            square.setHavePiece(true);
            this.updateKingsPosition(square);
        }else{
            square.setHavePiece(false);
        }
        return true;
    }
    
    public void consultCheckmate(boolean v){
        String message = DownTeam? "Time Branco" : "Time Preto";
        if(v){
            if(this.checkmateTest()){
                UIManager.put("OptionPane.okButtonText", "Clique aqui para encerrar o jogo");
                JOptionPane.showMessageDialog(null, message + ", seu Rei está morto",
                                            "Xeque-Mate!", JOptionPane.ERROR_MESSAGE);
                System.exit(0);
            }else{
                JOptionPane.showMessageDialog(null, message + ", você ainda pode salvar seu Rei",
                                            "Xeque!", JOptionPane.ERROR_MESSAGE);
            }
        }else{
            if(this.checkmateTest()){
                UIManager.put("OptionPane.okButtonText", "Clique aqui para encerrar o jogo");
                JOptionPane.showMessageDialog(null, "Não há movimentos válidos para o " + message,
                                            "Empate por Afogamento", JOptionPane.ERROR_MESSAGE);
                System.exit(0);
            }
        }
        this.pointList.clear();
        this.passantList.clear();
        this.castlingList.clear();
    }
    
    public void AutoCheck(Square square, Piece piece, boolean itMoved, WrongSquareException e){
        JOptionPane.showMessageDialog(null,
                              e.getMessage() + ", esse movimento deixa seu próprio Rei em xeque",
                              "Movimento Inválido", JOptionPane.WARNING_MESSAGE);
            
            this.selectedSquare.setPiece(square.getPiece());
            this.selectedSquare.setHavePiece(true);
            this.selectedSquare.getPiece().setItMoved(itMoved);
            this.updateKingsPosition(this.selectedSquare);
            
            if(piece != null){
                square.setPiece(piece);
                square.setHavePiece(true);
                this.updateKingsPosition(square);
            }else{
                square.setHavePiece(false);
            }
            this.unselectSquare(square);
    }
    
    public void executeCastling(Point squarePosition){
        Square square;
        int x = this.selectedSquare.getPosition().x;
        int y = this.selectedSquare.getPosition().y;
        switch(x){
            case 0:
                square = this.getSquare(y, x+3);
                this.moveContentOfSelectedSquare(square);
                this.selectedSquare = this.getSquare(y, x+4);
                square = this.getSquare(y, x+2);
                this.moveContentOfSelectedSquare(square);
                break;
                
            case 7:
                square = this.getSquare(y, x-2);
                this.moveContentOfSelectedSquare(square);
                this.selectedSquare = this.getSquare(y, x-3);
                square = this.getSquare(y, x-1);
                this.moveContentOfSelectedSquare(square);
                break;
                
            case 4:
                if(squarePosition.x == 0){
                    square = this.getSquare(y, x-2);
                    this.moveContentOfSelectedSquare(square);
                    this.selectedSquare = this.getSquare(y, x-4);
                    square = this.getSquare(y, x-1);
                    this.moveContentOfSelectedSquare(square);
                }else{
                    square = this.getSquare(y, x+2);
                    this.moveContentOfSelectedSquare(square);
                    this.selectedSquare = this.getSquare(y, x+3);
                    square = this.getSquare(y, x+1);
                    this.moveContentOfSelectedSquare(square);
                    break;
                }
                break;
        }
        this.DownTeam = !this.DownTeam;
    }
    
    public void executePassant(Point squarePosition){
        Square principalSquare = this.selectedSquare;
        int x = squarePosition.x;
        int y = squarePosition.y;
        Square square = this.getSquare(y, x);
        int operator = DownTeam? 1 : -1;
        boolean v;
        
        principalSquare.setHavePiece(false);
        square.setHavePiece(true);
        this.getSquare(y+operator, x).setHavePiece(false);
        square.setPiece(principalSquare.getPiece());
        
        try{
            v = checkTest();
        }catch(WrongSquareException e){
            JOptionPane.showMessageDialog(null,
                              e.getMessage() + ", esse movimento deixa seu próprio Rei em xeque",
                              "Movimento Inválido", JOptionPane.WARNING_MESSAGE);
            principalSquare.setHavePiece(true);
            square.setHavePiece(false);
            this.getSquare(y+operator, x).setHavePiece(true);
            this.unselectSquare(this.selectedSquare);
            
            return;
        }
        
        this.getSquare(y+operator, x).notifyOnChangeImagePath();
        square.notifyOnChangeImagePath();
        this.selectedSquare.notifyOnChangeImagePath();
        this.unselectSquare(this.selectedSquare);
        this.consultCheckmate(v);
        this.DownTeam = !this.DownTeam;
    }
    
    public void promotionTest(Square square){
        int y;
        if(square.getPiece().getClass() == Peao.class){
            y = square.getPosition().y;
            if(y == 7 || y == 0){
                Object[] options = {"Cavalo", "Bispo", "Torre", "Rainha"};
                Object ans = JOptionPane.showInputDialog(null, "Peão promovido!", "Deseja torná-lo qual peça?",
                                            JOptionPane.PLAIN_MESSAGE, null, options, "");
                Piece.Team team = y == 0? Piece.Team.DOWN_TEAM : Piece.Team.UP_TEAM;
                if(ans == "Cavalo"){
                    square.setPiece(new Cavalo(team, true));
                }else if(ans == "Bispo"){
                    square.setPiece(new Bispo(team, true));
                }else if(ans == "Torre"){
                    square.setPiece(new Torre(team, true));
                }else{
                    square.setPiece(new Rainha(team, true));
                }
                square.notifyOnChangeImagePath();
            }
        }
    }
}
