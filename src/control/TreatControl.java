package control;

import java.awt.Point;
import java.util.ArrayList;
import model.Bispo;
import model.Cavalo;
import model.Peao;
import model.Piece;
import model.Piece.Team;
import static model.Piece.Validate;
import model.Rainha;
import model.Rei;
import model.Square;
import model.Torre;

public abstract class TreatControl {  
    
    public static final int CHESSBOARD_ROW = 8;
    public static final int CHESSBOARD_COL = 8;
    
    public static void addTreat(Square square, Team principalTeam,
                                ArrayList<Point> treats, ArrayList<Point> temp,
                                Class<?> firstClass, Class<?> secondClass){
        if(square != null){
            if(square.havePiece()){
                if(square.getPiece().getTeam() != principalTeam){
                    if(square.getPiece().getTeam() != principalTeam){
                        if(square.getPiece().getClass() == firstClass ||
                        square.getPiece().getClass() == secondClass){
                            treats.add(square.getPosition());
                        }
                    }
                }
            }
        }
        temp.clear();
    }
    
    public static ArrayList<Point> treatControl(int x, int y, ArrayList<Square> squareList){
        ArrayList<Point> treats = new ArrayList<>();
        Square principalSquare = squareList.get(y*CHESSBOARD_COL+x);
        Team principalTeam = principalSquare.getPiece().getTeam();
        
        peaoTreat(x, y, squareList, principalTeam, treats);
        torreTreat(x, y, squareList, principalTeam, treats);
        bispoTreat(x, y, squareList, principalTeam, treats);
        cavaloTreat(x, y, squareList, principalTeam, treats);
        reiTreat(x, y, squareList, principalTeam, treats);
        
        return treats;
    }
    
    public static void peaoTreat(int x,int y, ArrayList<Square> squareList, 
                                Team principalTeam, ArrayList<Point> treats){
        int operator = principalTeam == Team.UP_TEAM? 1 : -1; 
        for(int i = -1; i <= 1; i += 2){
            if(Validate(x+i, y+operator)){
                Square square = squareList.get((y+operator)*CHESSBOARD_COL+x+i);
                if(square.havePiece()){
                    if(square.getPiece().getClass() == Peao.class){
                        if(square.getPiece().getTeam() != principalTeam){
                            treats.add(new Point(x+i, y+operator));
                        }
                    }
                }
            }
        }
    }
    
    public static void torreTreat(int x,int y, ArrayList<Square> squareList,
                                Team principalTeam, ArrayList<Point> treats){
        
        ArrayList<Point> temp = new ArrayList<>(); 
        Square square = null;
        Piece piece = new Torre(principalTeam, true);
     
        for (int i = y+1; i < CHESSBOARD_ROW; i++) {
            square = squareList.get(i*CHESSBOARD_COL+x);
            if(!piece.testPoint(square, x, i, temp)){
                break;
            }
        }
        
        addTreat(square, principalTeam, treats, temp, Torre.class, Rainha.class);
        
        
        for(int i = y-1; i >=0; i--){
            square = squareList.get(i*CHESSBOARD_COL+x);
            if(!piece.testPoint(square, x, i, temp)){
                break;
            }
        }
        addTreat(square, principalTeam, treats, temp, Torre.class, Rainha.class);
        
        
        for (int i = x+1; i < CHESSBOARD_COL; i++) {
            square = squareList.get(y*CHESSBOARD_COL+i);
            if(!piece.testPoint(square, i, y, temp)){
                break;
            }
        }
        addTreat(square, principalTeam, treats, temp, Torre.class, Rainha.class);
        
        for (int i = x-1; i >= 0; i--) {
            square = squareList.get(y*CHESSBOARD_COL+i);
            if(!piece.testPoint(square, i, y, temp)){
                break;
            }
        }
        addTreat(square, principalTeam, treats, temp, Torre.class, Rainha.class);
    }

    public static void bispoTreat(int x,int y, ArrayList<Square> squareList,
                                Team principalTeam, ArrayList<Point> treats){
        ArrayList<Point> temp = new ArrayList<>(); 
        Square square = squareList.get(0);
        Piece piece = new Torre(principalTeam, true);
        
        for (int i = y+1; Validate(i, x+i-y); i++) {
            square = squareList.get(i*CHESSBOARD_COL+x+i-y);
            if(!piece.testPoint(square, x+i-y, i, temp)){
                break;
            }
        }
        addTreat(square, principalTeam, treats, temp, Bispo.class, Rainha.class);
        
        for(int i = y-1; Validate(i, x+i-y); i--){
            square = squareList.get(i*CHESSBOARD_COL+x+i-y);
            if(!piece.testPoint(square, x+i-y, i, temp)){
                break;
            }
        }
        addTreat(square, principalTeam, treats, temp, Bispo.class, Rainha.class);
        
        for (int i = y+1; Validate(i, x-i+y); i++) {
            square = squareList.get(i*CHESSBOARD_COL+x-i+y);
            if(!piece.testPoint(square, x-i+y, i, temp)){
                break;
            }
        }
        addTreat(square, principalTeam, treats, temp, Bispo.class, Rainha.class);

        for (int i = y-1; Validate(i, x-i+y); i--) {
            square = squareList.get(i*CHESSBOARD_COL+x-i+y);
            if(!piece.testPoint(square, x-i+y, i, temp)){
                break;
            }
        }
        addTreat(square, principalTeam, treats, temp, Bispo.class, Rainha.class);
                
    }
    
    public static void cavaloTreat(int x,int y, ArrayList<Square> squareList,
                                Team principalTeam, ArrayList<Point> treats){
        ArrayList<Point> temp = new ArrayList<>(); 
        Square square = squareList.get(0);
        Piece piece = new Torre(principalTeam, true);
        
        if(Validate(x+1, y+2)){
            square = squareList.get((y+2)*CHESSBOARD_COL+x+1);
            piece.testPoint(square, x+1, y+2, temp);
        }
        addTreat(square, principalTeam, treats, temp, Cavalo.class, Cavalo.class);
        if(Validate(x+1, y-2)){
            square = squareList.get((y-2)*CHESSBOARD_COL+x+1);
            piece.testPoint(square, x+1, y-2, temp);
        }
        addTreat(square, principalTeam, treats, temp, Cavalo.class, Cavalo.class);
        if(Validate(x-1, y+2)){
            square = squareList.get((y+2)*CHESSBOARD_COL+x-1);
            piece.testPoint(square, x-1, y+2, temp);
        }
        addTreat(square, principalTeam, treats, temp, Cavalo.class, Cavalo.class);
        if(Validate(x-1, y-2)){
            square = squareList.get((y-2)*CHESSBOARD_COL+x-1);
            piece.testPoint(square, x-1, y-2, temp);
        }
        addTreat(square, principalTeam, treats, temp, Cavalo.class, Cavalo.class);
        if(Validate(x+2, y+1)){
            square = squareList.get((y+1)*CHESSBOARD_COL+x+2);
            piece.testPoint(square, x+2, y+1, temp);
        }
        addTreat(square, principalTeam, treats, temp, Cavalo.class, Cavalo.class);
        if(Validate(x+2, y-1)){
            square = squareList.get((y-1)*CHESSBOARD_COL+x+2);
            piece.testPoint(square, x+2, y-1, temp);
        }
        addTreat(square, principalTeam, treats, temp, Cavalo.class, Cavalo.class);
        if(Validate(x-2, y+1)){
            square = squareList.get((y+1)*CHESSBOARD_COL+x-2);
            piece.testPoint(square, x-2, y+1, temp);
        }
        addTreat(square, principalTeam, treats, temp, Cavalo.class, Cavalo.class);
        if(Validate(x-2, y-1)){
            square = squareList.get((y-1)*CHESSBOARD_COL+x-2);
            piece.testPoint(square, x-2, y-1, temp);
        }
        addTreat(square, principalTeam, treats, temp, Cavalo.class, Cavalo.class);
    }
    
    public static void reiTreat(int x,int y, ArrayList<Square> squareList,
                                Team principalTeam, ArrayList<Point> treats){
        ArrayList<Point> temp = new ArrayList<>(); 
        Square square = squareList.get(0);
        Piece piece = new Torre(principalTeam, true);
        
        if(Validate(x+1, y)){
            square = squareList.get(y*CHESSBOARD_COL+x+1);
            piece.testPoint(square, x+1, y, temp);
        }
        addTreat(square, principalTeam, treats, temp, Rei.class, Rei.class);
        if(Validate(x-1, y)){
            square = squareList.get(y*CHESSBOARD_COL+x-1);
            piece.testPoint(square, x-1, y, temp);
        }
        addTreat(square, principalTeam, treats, temp, Rei.class, Rei.class);
        if(Validate(x, y+1)){
            square = squareList.get((y+1)*CHESSBOARD_COL+x);
            piece.testPoint(square, x, y+1, temp);
        }
        addTreat(square, principalTeam, treats, temp, Rei.class, Rei.class);
        if(Validate(x, y-1)){
            square = squareList.get((y-1)*CHESSBOARD_COL+x);
            piece.testPoint(square, x, y-1, temp);
        }
        addTreat(square, principalTeam, treats, temp, Rei.class, Rei.class);
        if(Validate(x+1, y+1)){
            square = squareList.get((y+1)*CHESSBOARD_COL+x+1);
            piece.testPoint(square, x+1, y+1, temp);
        }
        addTreat(square, principalTeam, treats, temp, Rei.class, Rei.class);
        if(Validate(x+1, y-1)){
            square = squareList.get((y-1)*CHESSBOARD_COL+x+1);
            piece.testPoint(square, x+1, y-1, temp);
        }
        addTreat(square, principalTeam, treats, temp, Rei.class, Rei.class);
        if(Validate(x-1, y+1)){
            square = squareList.get((y+1)*CHESSBOARD_COL+x-1);
            piece.testPoint(square, x-1, y+1, temp);
        }
        addTreat(square, principalTeam, treats, temp, Rei.class, Rei.class);
        if(Validate(x-1, y-1)){
            square = squareList.get((y-1)*CHESSBOARD_COL+x-1);
            piece.testPoint(square, x-1, y-1, temp);
        }
        addTreat(square, principalTeam, treats, temp, Rei.class, Rei.class);
    }
}
