package tests.model;

import java.awt.Point;
import java.util.ArrayList;
import model.Bispo;
import model.Cavalo;

import model.Peao;
import model.Piece;
import model.Piece.Team;
import model.Rainha;
import model.Rei;
import model.Square;
import model.Torre;

import org.junit.Assert;
import org.junit.Test;

import tests.helper.MovesTestHelper;

public class PieceTest {

	private static final Class<?> CLASSE_PEAO = Peao.class;
	private static final Class<?> CLASSE_TORRE = Torre.class;
	private static final Class<?> CLASSE_CAVALO = Cavalo.class;
	private static final Class<?> CLASSE_BISPO = Bispo.class;
	private static final Class<?> CLASSE_REI = Rei.class;
	private static final Class<?> CLASSE_RAINHA = Rainha.class;

	@Test
	public void testMovePawn() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;
		ArrayList<Square> squareList = new ArrayList<>(64);
        Square square = new Square();
        square.setHavePiece(false);
        for(int i = 0; i < 64; i++){
        	squareList.add(square);
        }
                
		Piece pieceUpTeam = newPieceInstance(CLASSE_PEAO, Team.UP_TEAM);
		pieceUpTeam.setItMoved(true);
		Piece pieceDownTeam = newPieceInstance(CLASSE_PEAO, Team.DOWN_TEAM);
		pieceDownTeam.setItMoved(true);
		
		assertMoves(MovesTestHelper.getPawnMoves(Team.UP_TEAM),
				pieceUpTeam.getMoves(x, y, squareList));
		assertMoves(MovesTestHelper.getPawnMoves(Team.DOWN_TEAM),
				pieceDownTeam.getMoves(x, y, squareList));
	}

	@Test
	public void testMoveTower() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;
        ArrayList<Square> squareList = new ArrayList<>(64);
        Square square = new Square();
        square.setHavePiece(false);
        for(int i = 0; i < 64; i++){
        	squareList.add(square);
        }

		Piece piece = newPieceInstance(CLASSE_TORRE, Team.UP_TEAM);

		assertMoves(MovesTestHelper.getTowerMoves(), piece.getMoves(x, y, squareList));
	}

	@Test
	public void testMoveHorse() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;
		ArrayList<Square> squareList = new ArrayList<>(64);
        Square square = new Square();
        square.setHavePiece(false);
        for(int i = 0; i < 64; i++){
        	squareList.add(square);
        }
                
                
		Piece piece = newPieceInstance(CLASSE_CAVALO, Team.UP_TEAM);

		assertMoves(MovesTestHelper.getHorseMoves(), piece.getMoves(x, y, squareList));
	}

	@Test
	public void testMoveBishop() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;
		ArrayList<Square> squareList = new ArrayList<>(64);
        Square square = new Square();
        square.setHavePiece(false);
        for(int i = 0; i < 64; i++){
        	squareList.add(square);
        }

		Piece piece = newPieceInstance(CLASSE_BISPO, Team.UP_TEAM);

		assertMoves(MovesTestHelper.getBishopMoves(), piece.getMoves(x, y, squareList));
	}

	@Test
	public void testMoveKing() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;
		ArrayList<Square> squareList = new ArrayList<>(64);
        Square square = new Square();
        square.setHavePiece(false);
        for(int i = 0; i < 64; i++){
        	squareList.add(square);
        }
                
		Piece piece = newPieceInstance(CLASSE_REI, Team.UP_TEAM);

		assertMoves(MovesTestHelper.getKingMoves(), piece.getMoves(x, y, squareList));
	}

	@Test
	public void testMoveQueen() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;
		ArrayList<Square> squareList = new ArrayList<>(64);
        Square square = new Square();
        square.setHavePiece(false);
        for(int i = 0; i < 64; i++){
        	squareList.add(square);
        }

		Piece piece = newPieceInstance(CLASSE_RAINHA, Team.UP_TEAM);
		ArrayList<Point> moves = new ArrayList<Point>();

		moves.addAll(MovesTestHelper.getTowerMoves());
		moves.addAll(MovesTestHelper.getBishopMoves());

		assertMoves(moves, piece.getMoves(x, y, squareList));
	}

	private Piece newPieceInstance(Class<?> classPiece, Team team)
			throws Exception {
		return (Piece) classPiece.getDeclaredConstructor(Team.class)
				.newInstance(team);
	}

	private void assertMoves(ArrayList<Point> movesA, ArrayList<Point> movesB)
			throws Exception {
		if (movesA.size() != movesB.size()) {
			Assert.assertTrue(false);
			return;
		}

		for (Point point : movesA) {
			if (!movesB.contains(point)) {
				Assert.assertTrue(false);
				return;
			}
		}

		Assert.assertTrue(true);
	}
}
